public class produk {
    private String[] nama_produk;
    private int[] harga;
    private int[] qty;
    private byte id = -1;

    // !Setter and getter
    public String getNama_produk(int id) {
        return this.nama_produk[id - 1];
    }

    public int getHarga(int id) {
        return this.harga[id - 1];
    }

    public int getQty(int id) {
        return this.qty[id - 1];
    }

    public void minQty(int id, int minus) {
        this.qty[id - 1] = this.qty[id - 1] - minus;
    }

    public byte getTotalMenu() {
        return this.id;
    }
    // !Setter and getter

    public produk() {
        this.nama_produk = new String[100];
        this.harga = new int[100];
        this.qty = new int[100];
    }

    public void tambahProduk(String nama, int harga, int qyt) {
        this.id++;
        this.nama_produk[this.id] = nama;
        this.harga[this.id] = harga;
        this.qty[this.id] = qyt;
    }

    public void tampilProduk() {
        System.out.println("Daftar Menu Tersedia :\n====================================");
        for (int i = 0; i <= id; i++) {
            System.out.printf("%3d. %-20s = Rp. %d\tstok : %d\n", (i + 1), nama_produk[i], harga[i], qty[i]);
        }
    }
}
