public class penjualan {
    private String[] nama_produk;
    private int[] quantity;
    private int[] harga_total;
    private byte id = -1;

    public penjualan() {
        this.nama_produk = new String[100];
        this.quantity = new int[100];
        this.harga_total = new int[100];
    }

    public void tampilBag() {
        System.out.println("\nKeranjang pembelian :\n================================");
        int TotalBelanja = 0;
        for (int i = 0; i <= id; i++) {
            System.out.printf("%3d. %-20s (%d) -> Rp. %d\n", i + 1, nama_produk[i], quantity[i], harga_total[i]);
            TotalBelanja = TotalBelanja + harga_total[i];
        }
        System.out.println("\nTotal:\tRp. " + TotalBelanja);
    }

    public void tambahBelanja(String nama_produk, int harga, int quantity) {
        this.id++;
        this.nama_produk[id] = nama_produk;
        this.harga_total[id] = harga * quantity;
        this.quantity[id] = quantity;
    }
}
