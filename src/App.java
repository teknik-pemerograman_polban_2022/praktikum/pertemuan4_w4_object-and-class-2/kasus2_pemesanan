import java.util.Scanner;

public class App {
    private static boolean isInputValid(int maxRange, int input) {
        if (input > maxRange || input <= 0) {
            System.out.println("input tidak valid!!\ntekan enter untuk melanjutkan");
            try {
                System.in.read();
            } catch (Exception e) {
            }
            return false;
        } else {
            return true;
        }
    }

    private static void tampilHUD(produk Menu, penjualan Bag) {
        System.out.print("\033[H\033[2J");
        System.out.flush();
        Menu.tampilProduk();
        Bag.tampilBag();
    }

    private static void initializeMenu(produk menu) {
        menu.tambahProduk("Batagor", 1_000, 5);
        menu.tambahProduk("Basreng", 2_000, 10);
        menu.tambahProduk("Cilok", 2_000, 10);
        menu.tambahProduk("Gehu", 5_000, 10);
        menu.tambahProduk("Bakso", 5_000, 10);
        menu.tambahProduk("Mixue", 18_000, 10);
        menu.tambahProduk("Teh", 10_000, 10);
        menu.tambahProduk("AQUA", 3_000, 10);
        menu.tambahProduk("RON 88", 3_000, 10);
        menu.tambahProduk("Le Mineral", 3_000, 10);
        menu.tambahProduk("Teh Sosro", 3_000, 10);
    }

    public static void main(String[] args) throws Exception {
        produk Produk = new produk();
        penjualan Bag = new penjualan();
        Scanner kScanner = new Scanner(System.in);
        char PesanLagi = 'Y';
        int PilihanMakanan = 0;
        int qtyDipesan = 0;

        initializeMenu(Produk);
        while (Character.toUpperCase(PesanLagi) == 'Y') {
            tampilHUD(Produk, Bag);
            System.out.println("\nPilih nomor menu yang ingin dipesan : ");
            PilihanMakanan = kScanner.nextInt();
            if (!isInputValid(Produk.getTotalMenu(), PilihanMakanan)) {
                continue;
            }
            System.out.println("\nMasukan jumlah qty : ");
            qtyDipesan = kScanner.nextInt();
            if (!isInputValid((Produk.getQty(PilihanMakanan)), qtyDipesan)) {
                continue;
            }
            Bag.tambahBelanja(Produk.getNama_produk(PilihanMakanan), Produk.getHarga(PilihanMakanan), qtyDipesan);
            Produk.minQty(PilihanMakanan, qtyDipesan);
            tampilHUD(Produk, Bag);
            System.out.println("\ningin memesan lagi? Y/N\n");
            PesanLagi = kScanner.next().charAt(0);
        }
        kScanner.close();
    }
}